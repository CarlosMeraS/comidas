$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval:2200	
			})
		}) 
		$('#opinion').on('show.bs.modal', function(e){
			console.log('El modal de opinión se está mostrando');
			$('#opinionBtn').removeClass('btn-outline-success');
			$('#opinionBtn').addClass('btn-primary');
			$('#opinionBtn').prop('disabled', true);
		});
		$('#opinion').on('shown.bs.modal', function(e){
			console.log('El modal de opinión ya no se está mostrando');
		});
		$('#opinion').on('hide.bs.modal', function(e){
			console.log('El modal de opinión se está ocultando');
		});
		$('#opinion').on('hidden.bs.modal', function(e){
			console.log('El modal de opinión se ocultó');
			$('#opinionBtn').removeClass('btn-primary');
			$('#opinionBtn').addClass('btn-outline-success');
			$('#opinionBtn').prop('disabled', false);
		});